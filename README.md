# [Naza Alakija - Landing Page]


## Download and Installation

Clone the repo to a folder on your server: git clone https://gitlab.com/digitalbrush/naza-alakija.git

## Usage

### Basic Usage

After downloading, simply edit the HTML and SCSS files included with the template in your favorite text editor to make changes. The only files you need to worry about are sass/_custom.scss, js/scripts.js and index.html, you can ignore everything else.

### Advanced Usage

After installation, run `npm install` and then run `npm start` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved. You can view the `gulpfile.js` to see which tasks are included with the dev environment.

You must have npm and Gulp installed globally on your machine in order to use these features.


