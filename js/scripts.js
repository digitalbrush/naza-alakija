/*!
    * Start Bootstrap - Grayscale v6.0.2 (https://startbootstrap.com/themes/grayscale)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-grayscale/blob/master/LICENSE)
    */

(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top - 70,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    jQuery(document).ready(function($){
        orientationChange();
    });
    
    function orientationChange() {
        if(window.addEventListener) {
            window.addEventListener("orientationchange", function() {
                location.reload();
            });
        }
    }

    // Closes responsive menu when a scroll trigger link is clicked
    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#mainNav",
        offset: 100,
    });

    $(document).ready(function() {
        if(window.matchMedia("(max-height: 420px)").matches){
            $('section').addClass('fp-auto-height-responsive');
            $('header').addClass('fp-auto-height-responsive');
            $('.about-section').find('div.container').removeClass('container').addClass('container-fluid');
            $('.humanitarian-section').find('div.container').removeClass('container').addClass('container-fluid');
        }
        else {
            $('section').removeClass('fp-auto-height-responsive');
            $('header').removeClass('fp-auto-height-responsive');
            $('.about-section').find('div.container-fluid').removeClass('container-fluid').addClass('container');
            $('.humanitarian-section').find('div.container-fluid').removeClass('container-fluid').addClass('container');
        }
    });

    // variables
    var $nav = $('#mainNav');

    $('#fullpage').fullpage({
        sectionSelector: '.vertical-scrolling',
        navigation: true,
        controlArrows: false,
        anchors: ['welcome', 'about', 'humanitarian', 'environmentalist', 'philanthropist', 'contact'],
        scrollingSpeed: 700,
        navigation: false,
        animateAnchor: true,
        scrollBar: false,
        scrollingSpeed: 700,
        normalScrollElements: '.modal',
        autoScrolling: true,
        fitToSection: true,
        responsiveHeight: 420,
        licenseKey: 'CD64714F-DAC24521-B811496E-7F2AF398',

        afterLoad: function(origin, destination, direction){
            var loadingSection = this;

            if(destination.anchor == 'welcome'){
                $nav.find('a').css(
                    {
                        'color': '#2B1919',
                    }
                );
                $nav.find('.line').css('background', '#2B1919');
            }
            if(destination.anchor == 'about'){
                $nav.find('a').css(
                    {
                        'color': '#342323',
                    }
                );
                $nav.find('.line').css('background', '#342323');
            }
            if(destination.anchor == 'humanitarian'){
                $nav.find('a').css(
                    {
                        'color': '#fff',
                    }
                );
                $nav.find('.line').css('background', '#fff');
            }
            if(destination.anchor == 'environmentalist'){
                $nav.find('a').css(
                    {
                        'color': '#fff',
                    }
                );
                $nav.find('.line').css('background', '#fff');
            }
            if(destination.anchor == 'philanthropist'){
                $nav.find('a').css(
                    {
                        'color': '#885F4F',
                    }
                );
                $nav.find('.line').css('background', '#885F4F');
            }
            if(destination.anchor == 'contact'){
                $nav.find('a').css(
                    {
                        'color': '#FEF8F3',
                    }
                );
                $nav.find('.line').css('background', '#FEF8F3');
            }
        }
    });

    $('.nav-menu-icon').on('click', function () {
        $(this).toggleClass('active animate__fadeInLeft');
        if(window.matchMedia("(max-width: 767px)").matches){
            $('.nav-menu-icon').parent().parent().siblings().find('.navbar-brand').toggleClass('animate__fadeOutRight');
        }
    });

    // collapse Navbar when menu item is selected
    $('.nav-item').on('click', function (e) {
        e.preventDefault();
        $('.nav-menu-icon').toggleClass('active animate__fadeInLeft');
        if(window.matchMedia("(max-width: 565px)").matches){
            $('.nav-menu-icon').parent().parent().siblings().find('.navbar-brand').toggleClass('animate__fadeOutRight');
        }
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
})(jQuery); // End of use strict
